//
//  SplitViewController.swift
//  CardEngineEditor
//
//  Created by Karthik Bedre on 4/26/18.
//  Copyright © 2018 T-Mobile. All rights reserved.
//

import UIKit

class SplitViewController: UISplitViewController {

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setConfiguration()
    }
}

extension SplitViewController {
    func setConfiguration() {
        preferredDisplayMode = .allVisible
        maximumPrimaryColumnWidth = self.view.frame.size.width
        minimumPrimaryColumnWidth = self.view.frame.size.width*0.65
    }
}
