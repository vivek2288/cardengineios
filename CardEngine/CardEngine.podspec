Pod::Spec.new do |s|

  s.name         	= "CardEngine"
  s.version      	= "1.0.0"
  s.platform        = :ios, "10.0"
  s.summary      	= "CardEngine is a iOS dynamic framwork which provides convenient way to display cards."
  s.homepage     	= "https://bitbucket.corporate.t-mobile.com/scm/tma/tmo_connection_ios-sdk.git"
  s.author       	= { "kbedre" => "karthik.bedre1@t-mobile.com" }
  s.source       	= { :git => "https://bitbucket.corporate.t-mobile.com/scm/tma/tmo_connection_ios.git", :tag => "#{s.version}" }
  s.source_files  	= "CardEngine/**/*.{swift,c,h,m}"
  s.resources 		= "CardEngine/**/*.{png,jpeg,jpg,storyboard,xib,json,otf}"

end
